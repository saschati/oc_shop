<?php
class ModelToolCsv extends Model {
    public $csv_file = array();
    public function getTotalProductAndManufacturerForCsv() {
        $query = $this->db->query("SELECT m.manufacturer_id as id_url, m.name as name_brend ,count(p.model) as product_count, 'count_stock' FROM " .DB_PREFIX . "manufacturer m LEFT JOIN  " .DB_PREFIX . "product p ON p.manufacturer_id = m.manufacturer_id GROUP by m.name");
        foreach($query->rows as $result)
                $this->csv_file[] = $result;

        return $this->csv_file;
    }
    public function getArreyMerge()
    {
        $query = $this->db->query("SELECT m.name as name_brend, Count(ss.stock_status_id) as count_stock FROM " . DB_PREFIX . "product p INNER JOIN " . DB_PREFIX . "manufacturer m ON p.manufacturer_id = m.manufacturer_id
                                                                    INNER JOIN " . DB_PREFIX . "stock_status ss ON p.stock_status_id = ss.stock_status_id
WHERE ss.language_id = 1
AND p.stock_status_id = 7
GROUP by m.name");
        foreach ($query->rows as $item) {
            for ($i = 0; $i < count($this->csv_file); $i++) {
                if ($this->csv_file[$i]['name_brend'] == $item['name_brend']) {
                    $this->csv_file[$i]['count_stock'] = $item['count_stock'];
                }
                elseif ($this->csv_file[$i]['count_stock'] == 'count_stock' ){
                    $this->csv_file[$i]['count_stock'] = 'null';
                }
            }

        }
        return $this->csv_file;
    }

    public function  getAddUrl($url){
        $url =   str_replace('admin/', '', $url[0]);
        foreach ($this->csv_file as $item){
            $url[$item['id_url']]['name_brend'] = $item['name_brend'];
            $url[$item['id_url']]['url_name'] = $url['url'].$item['id_url'];
        }
        array_shift($url);

        foreach ($url as $link){
            for ($i = 0; $i < count($this->csv_file); $i++) {
                if ($this->csv_file[$i]['name_brend'] == $link['name_brend']) {
                    $this->csv_file[$i]['url_name'] = $link['url_name'];
                    unset($this->csv_file[$i]['id_url']);
                }
               }
           }
       return $this->csv_file;
    }


    public function  saveCsvFile(){
        $filename = 'upload/'.time().'.csv';
        $fp = fopen($filename,"w");
        $i = 0;
        $seperator = array("Brend","Count product","product in stock","url");
        foreach ($this->csv_file as $name => $value){
            if($i == 0) {
                fputcsv($fp, $seperator, ';');
                fputcsv($fp, $value, ';');
            }
                else
            fputcsv($fp,  $value,';' );
            $i++;
        }
       fclose( $fp);
    }

}




//SELECT m.name as Brend, COUNT(p.model) as count_product FROM oc_product p LEFT JOIN oc_manufacturer m ON p.manufacturer_id = m.manufacturer_id GROUP BY m.name
//--------------
//SELECT oc_manufacturer.name as brends, COUNT(oc_product.model) as count_product FROM  oc_product LEFT JOIN  oc_manufacturer ON oc_product.manufacturer_id = oc_manufacturer.manufacturer_id GROUP BY  oc_manufacturer.name
//--------
//SELECT m.name as brends, COUNT(p.model) as count_product FROM " . DB_PREFIX . " product p LEFT JOIN " .DB_PREFIX . " manufacturer m ON (p.manufacturer_id = m.manufacturer_id) GROUP BY m.name"
//----------
//SELECT m.name, COUNT(p.model), ss.stock_status_id FROM oc_product p INNER JOIN oc_manufacturer m ON p.manufacturer_id = m.manufacturer_id
//                                                                    INNER JOIN oc_stock_status ss ON p.stock_status_id = ss.stock_status_id
//WHERE ss.language_id = 1
//AND p.stock_status_id = 7
//GROUP by m.name