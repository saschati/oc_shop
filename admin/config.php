<?php
// HTTP
define('HTTP_SERVER', 'http://opencart.loc/admin/');
define('HTTP_CATALOG', 'http://opencart.loc/');

// HTTPS
define('HTTPS_SERVER', 'http://opencart.loc/admin/');
define('HTTPS_CATALOG', 'http://opencart.loc/');

// DIR
define('DIR_APPLICATION', 'E:/OSPanel/domains/opencart.loc/admin/');
define('DIR_SYSTEM', 'E:/OSPanel/domains/opencart.loc/system/');
define('DIR_LANGUAGE', 'E:/OSPanel/domains/opencart.loc/admin/language/');
define('DIR_TEMPLATE', 'E:/OSPanel/domains/opencart.loc/admin/view/template/');
define('DIR_CONFIG', 'E:/OSPanel/domains/opencart.loc/system/config/');
define('DIR_IMAGE', 'E:/OSPanel/domains/opencart.loc/image/');
define('DIR_CACHE', 'E:/OSPanel/domains/opencart.loc/system/storage/cache/');
define('DIR_DOWNLOAD', 'E:/OSPanel/domains/opencart.loc/system/storage/download/');
define('DIR_LOGS', 'E:/OSPanel/domains/opencart.loc/system/storage/logs/');
define('DIR_MODIFICATION', 'E:/OSPanel/domains/opencart.loc/system/storage/modification/');
define('DIR_UPLOAD', 'E:/OSPanel/domains/opencart.loc/system/storage/upload/');
define('DIR_CATALOG', 'E:/OSPanel/domains/opencart.loc/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'ocstore');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
