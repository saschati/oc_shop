<?php
class ControllerModuleCsv extends Controller {
    private $error = array();
    public function index()
    {

        $this->load->language('module/csv');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('tool/csv');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('csv', $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
            $data['success'] = $this->session->data['success'];

        }

        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_success'] = $this->language->get('text_success');
        $data['text_module'] = $this->language->get('text_module');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['error_permission'] = $this->language->get('error_permission');
        $data['text_content'] = $this->language->get('text_content');


        $data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/csv', 'token=' . $this->session->data['token'], 'SSL')
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_brand'),
            'href' => $this->url->link('product/manufacturer')
        );
        $data['url_manifakture'][] = array(
            'url' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' )
        );

        $data['action'] = $this->url->link('module/csv', 'token=' . $this->session->data['token'], 'SSL');
        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

        $data['csv_arrey'] = array();
        $this->load->model('tool/csv');
        $data['csv_arrey'] = $this->model_tool_csv->getTotalProductAndManufacturerForCsv();
        $data['csv_arrey'] = $this->model_tool_csv->getArreyMerge();
        $data['csv_arrey'] = $this->model_tool_csv->getAddUrl($data['url_manifakture']);

        if(isset($this->request->post['csv'])){
            $data['csv_file'] = $this->model_tool_csv->saveCsvFile();
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');



        $this->response->setOutput($this->load->view('module/csv.tpl', $data));
    }
    protected function validate() {
        if (!$this->user->hasPermission('modify', 'module/csv')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        return !$this->error;
    }
}


